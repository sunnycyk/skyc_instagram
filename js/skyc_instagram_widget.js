// JavaScript Document

if ( typeof Object.create !== 'function' ) {
	Object.create = function( obj ) {
		function F() {};
		F.prototype = obj;
		return new F();
	};
}

(function($, window, document) {
	
	var skycInstagramWidget = {
		init: function(options, elem) {
			var self=this;
			self.elem = elem;
			self.container = $(elem);
			self.options = $.extend({}, $.fn.skycInstagramWidget.options, options);
			self.start();
		},
		start: function() {
			var self = this,
			
			query = {
					feedType: self.options.feedType,
					postPerPage: self.options.postPerPage,
					nonce: self.options.nonce,
					action: self.options.action
			};
			//feed = self.container.find('#' + self.options.feedType);
			self.container.find('.loading').show();
			$.ajax({
					type: "POST",
					data: query,
					dataType : "xml",
					url: self.options.ajaxURL,
					success: function(data) {
						self.container.find('.loading').remove();
						var $data = $(data);
						var status = $data.find('response_data').text();
						if (status == 'success') {
								var images = $data.find(self.options.feedItems).text(),
									imgWidth = Math.floor(self.container.width()/(self.options.column+0.5));
									
								// Process Images
								images = $.parseJSON(images);
								
								
								
							switch (self.options.feedType) {
								case 'userfeed':
								case 'recentfeed':
								case 'popularfeed':
								
										for (var i=0; i < images.length; i++) {
										
												$('<img />').appendTo(self.container).attr({
													src: images[i].images.thumbnail.url,
													width: imgWidth,
												}).css('padding','1px').fadeIn();
										}
										break;
								case 'followers':
								case 'following':
									for (var i=0; i < images.length; i++) {
										
										$('<img />').appendTo(self.container).attr({
											src: images[i].profile_picture,
											width: imgWidth,
										}).css('padding','1px').fadeIn();
									}
									break;
							}
								
						}
						else {
							self.container.append('<p>' + $data.find(self.options.errorItems).text() + '</p>');
						}
						
						
					},
					error: function(msg){
						self.container.find('.loading').remove();
						self.container.append('<p>Error Fetching data: ' + msg + '</p>');
					}
					
				});
		}
	
	};
	
	$.fn.skycInstagramWidget = function(options) {
		return this.each(function() {
			var instagramWidget = Object.create(skycInstagramWidget);
			
			instagramWidget.init(options, this);
		});
	}
	
	$.fn.skycInstagramWidget.options = {
		feedType: 'recentfeed',
		postPerPage: 20,
		nonce: '',
		action: 'skycInstagram_widget_action',
		feedItems: 'supplemental images',
		errorItems: 'supplemental reason',
		ajaxURL: 'admin-ajax.php',
		column: 4
	};
})(jQuery, window, document);

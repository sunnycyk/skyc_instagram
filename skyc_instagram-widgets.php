<?php

/** 
 *   Script for Widget
 */
 // Recent Feed Widget
 class skycInstagram_Feed_Widget extends WP_Widget {
	 
	 function skycInstagram_Feed_Widget() {
	 	$widget_ops = array(
			'classname' => 'skycInstagram_wiget_class',
			'description' => 'Display recent feed photo for user'
		);
		$this->WP_Widget('skycInstagram_Recent_Feed_Widget', 'Instagram Photos', $widget_ops);
	 }
	 
	 function form($instance) {
	 	$defaults = array('title' => 'Instagram Photo', 'postPerPage' => "20", 'FeedType' => 'Recent Feed');
		
		$instance = wp_parse_args((array)$instance, $defaults);
		
		$title = $instance['title'];
		$postPerPage = $instance['postPerPage'];
		$feedType = $instance['FeedType'];
?>
		<p>Title: <input name="<?php echo $this->get_field_name('title');?>" class="widefat" type="text" value="<?php echo $title;?>" /></p>
		<p>Post Per Page(max 20): <input class="widefat" name="<?php echo $this->get_field_name('postPerPage'); ?>" type="text" value="<?php echo $postPerPage;?>" /></p>
		<p>Feet Type: 
		<select name="<?php echo $this->get_field_name('FeedType'); ?>">
			<option value="recentfeed" <?php selected($feedType, "recentfeed"); ?>>Recent Feed</option>
			<option value="userfeed" <?php selected($feedType, "userfeed"); ?>>User Feed</option>
			<option value="followers" <?php selected($feedType, "followers"); ?>>Your Followers</option>
			<option value="following" <?php selected($feedType, "following"); ?>>Following</option>
			<option value="popularfeed" <?php selected($feedType, "popularfeed"); ?>>Popular</option>
		</select>
		</p>
<?php	
	 }
	 
	 function update($new_instance, $old_instance) {
	 	$instance = $old_instance;
		
		
		if ($new_instance['postPerPage'] <= 0 || $new_instance['postPerPage'] > 20)
			$instance['postPerPage'] = 20;
		else
			$instance['postPerPage'] = strip_tags($new_instance['postPerPage']);
		$instance['title'] = strip_tags($new_instance['title']);
		$instance['FeedType'] = strip_tags($new_instance['FeedType']);
		
		return $instance;
	 }
	 
	 function widget($args, $instance) {
		 extract($args);
		 echo $before_widget;
		 
		 $title = apply_filters('widget_title', $instance['title']);
		 
		 if (!empty($title) ) {
			 echo $before_title.$title.$after_title;
		 }
		
		 switch ($instance['FeedType']) {
		 	case 'recentfeed': echo '<div id="recentfeed"><div class="loading"></div></div>'; 
								
								break;
			case 'userfeed': echo '<div id="userfeed"><div class="loading"></div></div>';
							 
							  break;

			case 'followers':echo '<div id="followers"><div class="loading"></div></div>'; break;
			case 'following':echo '<div id="following"><div class="loading"></div></div>'; break;
			case 'popularfeed':echo '<div id="popularfeed"><div class="loading"></div></div>'; break;
		 
		 }
		 $protocol = isset($_SERVER['HTTPS']) ? 'https://' : 'http://';
		 $nonce = wp_nonce_url(admin_url('admin.php?page=skycInstagram_plugin'), 'skycInstagram_widget_action' );
		 
		
?>
<script type="text/javascript">
jQuery(document).ready(function($) {
	var link = '<?php echo $nonce; ?>',
		link = link.replace(/^.*_wpnonce=([a-z0-9]+).*$/, '$1');
	$('#<?php echo $instance['FeedType']; ?>').skycInstagramWidget({
			feedType: '<?php echo $instance['FeedType']; ?>',
			postPerPage: <?php echo $instance['postPerPage']; ?>,
			nonce: link,
			action: 'skycInstagram_widget_action',
			feedItems: 'supplemental images',
			errorItems: 'supplemental reason',
			ajaxURL: '<?php echo admin_url('admin-ajax.php', $protocol); ?>',
			column: 5
	});
});
</script> 
<?php
		 echo $after_widget;
		 
	 }
 }


// Register Widget 
add_action('widgets_init', 'skycInstagram_register_widgets');

function skycInstagram_register_widgets() {
	if (get_option('skycInstagramAccessToken')) {
	 	register_widget('skycInstagram_Feed_Widget');
	}
}
	 
 ?>
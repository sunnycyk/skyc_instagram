<?php
/* 
Plugin Name:  Sunny's Instagram 
Plugin URI: http://sunnycyk.hk	
Description: Allow you to promote your Instagram photo on your WordPress website.  
Author: Sunny Cheung	
Version: 1.00
Author URI: http://sunnycyk.hk/skyc_instagram
*/

require_once(plugin_dir_path(__FILE__).'skyc_instagram-widgets.php'); // widgets

// Activation Prepare
register_activation_hook(__FILE__, 'skycInstagram_install');

function skycInstagram_install() {
	// Add all option for use later in plugin
	// This client ID is custom for this plugin
	add_option('skycInstagramClientID', 'd3da1709405e42e1a13ece51935d0ea2');
	add_option('skycInstagramUser', false);
	add_option('skycInstagramAccessToken', false);
	add_option('skycInstagramRedirectURI', 'http://wp-test.local/skyc_instagram/redirectURI.php');
}


// Deactivation 
register_deactivation_hook(__FILE__, 'skycInstagram_uninstall');

function skycInstagram_uninstall() {
	delete_option('skycInstagramClientID');
	delete_option('skycInstagramUser');
	delete_option('skycInstagramAccessToken');
	delete_option('skycInstagramRedirectURI');
}


// Add Menu
add_action('admin_menu', 'skycInstagram_menu');

function skycInstagram_menu() {
	add_menu_page(__('Sunny\'s Instagram Setting Page'), __('Sunny\'s Instagram Setting'), 'manage_options','skycInstagram_plugin',	
		'skycInstagram_setting_page'); 
}

function skycInstagram_setting_page() {
?>
	<div class="wrap">
		<header>
			<?php screen_icon('tools');?>
			<h2>Sunny's Instagram Gallery Settings</h2>
			 <?php settings_errors(); ?>
		</header>
<?php
	if (isset($_GET['access_token']) && isset($_GET['user'])) {
		delete_option('skycInstagramAccessToken');
		add_option('skycInstagramAccessToken', esc_attr($_GET['access_token']));	
		delete_option('skycInstagramUser');
		add_option('skycInstagramUser', $_GET['user']);
	}
	
	if ($user = get_option('skycInstagramUser')) 
		$user = unserialize(stripslashes($user));
	
	if (!$user) {
		
		if ($_GET['access_token'] == "") {
			echo '<div class="error">You have to authorize from Instagram before setting this plugin</div>';
		}
?>
		<div>
			<form action="https://api.instagram.com/oauth/authorize/?" method="get">
				<input name="client_id" type="hidden" value="<?php echo get_option('skycInstagramClientID'); ?>"/>
				<input name="response_type" type="hidden" value="code" />
				<input name="scope"type="hidden" value="basic comments relationships likes" />
				<input name="redirect_uri" type="hidden" value="http://wp-test.local/skyc_instagram/redirectURI.php?redirect_uri=<?php echo base64_encode( get_admin_url().'admin.php?page=skycInstagram_plugin'); ?>" />
				<input name="Submit" type="submit" value="Authorized from Instagram" class="button-primary" />
			</form>
		</div>
<?php
	}
	else { // user information found
		// Do I need to do some basic checking to verify access token is valid?
		

	}
?>
	</div>
<?php
}

/** 
 * Add Notification if access token is not set
 */
function skycInstagramAdmin_notice() {
	if (!get_option('skycInstagramAccessToken') && !isset($_GET['access_token'])) {
		echo '<div class="error">You have activated Instagram Plugin, but you have not finish the setup yet.  Please <a href="admin.php?page=skycInstagram_plugin">authorize</a>
			Instagram Plugin. </div>';
	}
}
add_action('admin_notices', 'skycInstagramAdmin_notice');

/** 
 * Ajax Function for widget
 */
 
add_action('wp_ajax_skycInstagram_widget_action', "skycInstagram_widget_action");
add_action('wp_ajax_nopriv_skycInstagram_widget_action', "skycInstagram_widget_action");

function skycInstagram_widget_action() {
	
	 $feedType = strip_tags($_POST['feedType']);
	 $postPerPage = strip_tags($_POST['postPerPage']);
	 
	 check_ajax_referer('skycInstagram_widget_action', 'nonce', false);

	 $response = new WP_Ajax_Response;
	 $url = 'https://api.instagram.com/v1/';
	 if ($accessToken = get_option('skycInstagramAccessToken')) {
		 $user = get_option('skycInstagramUser');
		 $user = unserialize(stripslashes($user));
	 	switch ($feedType) {
			case 'recentfeed': 	$url .= 'users/'.$user->id.'/media/recent/?access_token='.$accessToken.'&count='.$postPerPage;
								break;
			case 'userfeed': $url .= 'users/self/feed?access_token='.$accessToken.'&count='.$postPerPage;
							 break;				
			case 'followers': $url .= 'users/'.$user->id.'/followed-by?access_token='.$accessToken.'&count='.$postPerPage; break;
			case 'following':$url .= 'users/'.$user->id.'/follows?access_token='.$accessToken.'&count='.$postPerPage;
			case 'popularfeed':$url .= 'media/popular?access_token='.$accessToken.'&count='.$postPerPage;break;
		 
		}
		$args = array(
			'method' => 'GET',
			'sslverify' => false,
		);
		$data = wp_remote_get($url, $args);
		if (is_wp_error($data)) { // Error
			$response->add( 
				array(
					'data' => 'error',   
					'supplemental' => array(
						'reason' => 'Cannot fetch feed from Instagram'
					)
				)
			);
	 		$response->send();
	 		die();
		}
		else {
			if ($data['response']['code'] == 200) { // code ok
				$json_obj = json_decode($data['body']);
				
				
				$response->add(
					array(
						'data' => 'success',
						'supplemental' => array(
								'images' => json_encode($json_obj->data)
							)	
					)
				);
				
			}
			else {
				$response->add( 
					array(
						'data' => 'error',   
						'supplemental' => array(
							'reason' => 'Error.  Cannot receive from Instagram.  Access Token maybe revoked!'
						)
					)
				);
	 			
			}
			$response->send();
			die();
		}
	 
	 }
	 $response->add( 
			array(
				'data' => 'error',   
				'supplemental' => array(
					'reason' => 'Access token is not found! Cannot authorise Instagram'
				)
			)
	);
	 $response->send();
	 die();
}

add_action('wp_enqueue_scripts', 'skycInstagramWidgetScripts');

function skycInstagramWidgetScripts() {
	 wp_enqueue_script('jquery');
	 wp_enqueue_script('skycInstagramScript', path_join(WP_PLUGIN_URL, basename(dirname(__FILE__)).'/js/skyc_instagram_widget.js'), array('jquery'));
	 wp_enqueue_style('skycInstagramStyle',  path_join(WP_PLUGIN_URL, basename(dirname(__FILE__)).'/css/skyc_instagram.css'));
}